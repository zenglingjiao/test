<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    // 指定连接
    protected $connection = 'mysql';
    //指定表
    protected $table='user';
    //指定主键
    protected $primaryKey='id';
    //禁止操作时间
    public $timestamps=false;
    //设置允许写入的数据字段
    protected $fillable =['id','name'];
}
