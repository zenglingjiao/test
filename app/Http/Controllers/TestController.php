<?php

namespace App\Http\Controllers;

use App\Admin\User;
use DB;
//引入模型

class TestController extends Controller
{
    //
    public function test1()
    {
        $col = DB::collection('col');
        // $col->insert(['name'=>'a','age'=>11]);
        return $col->get();
//         $res=$col->get();
        //         return $res;
        // return phpinfo();
    }
    public function demo()
    {
        // $res = DB::table($this->table_name)->connection($this->connection)->where('regip','=','0.0.0.0')->get();
        // return $res->toArray();

        //分页
        $res = DB::connection('mysql')->table('user')->simplePaginate(2);
        // return view('admin/demo', ['res' => $res]);
        return view('admin/demo', compact('res'));
    }
    public function get()
    {
        //
        // $res=DB::connection('mysql')->table('user')->get();
        $res = DB::connection('mysql')->table('user')->limit(1)->offset(0)->get();
        return $res;
    }

    //循环
    public function test3()
    {
        $res = DB::connection('mysql')->table('user')->get();
        return view('admin/test3', compact('res'));
        /*
    php中的写法
    foreach ($variable as $key => $value) {

    }

    if(){

    }elseif(){

    }else{

    }

    视图中的写法
    循环
    @foreach ($variable as $key => $value)

    @foreach
    判断
    @if()

    @elseif()

    @else

    @endif
     */

    }

    //CSRF验证
    public function test4()
    {
        return view('admin/test4');
    }
    public function test5()
    {
        return 'hello';
    }
    public function test6()
    {

        $user       = new User;
        $user->name = '哈哈';
        $res        = $user->save();
        dd($res);
    }
    public function test7()
    {

    }
}
