<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    // echo phpinfo();
    return view('welcome');
});
//分组路由
Route::group(['prefix' => 'admin/test'], function () {
    Route::get('test1', 'TestController@test1');
    Route::get('demo', 'TestController@demo');
    Route::get('get', 'TestController@get');
    Route::get('test3', 'TestController@test3');
    //csrf验证
    Route::get('test4', 'TestController@test4');
    Route::post('test5', 'TestController@test5');

    //ar模型
    Route::get('test6', 'TestController@test6');
    Route::get('test7', 'TestController@test7');
    Route::get('test8', 'TestController@test8');
    Route::get('test9', 'TestController@test9');
});

Route::get('/home/index/index', 'Home\IndexController@index');
Route::get('/admin/index/index', 'Admin\IndexController@index');
